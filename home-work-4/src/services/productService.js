let currentId = 2000; // Чтобы не пересекались ID с fake
let localProducts = []; // Локально добавленные продукты
let allProducts = []; // Кэш всех продуктов (с сервера и локальных)

// Получение продуктов (загрузка с сервера и добавление локальных)
export async function fetchProducts() {
    if (allProducts.length > 0) {
        return allProducts; // Возвращаем кэш, если данные уже загружены
    }

    const response = await fetch('https://fakestoreapi.com/products');
    if (!response.ok) {
        throw new Error('Failed to fetch products');
    }

    let products = await response.json();
    products = applyRandomDiscounts(products);
    allProducts = [...products, ...localProducts]; // Сохраняем в кэш
    return allProducts;
}

// Применение случайных скидок
function applyRandomDiscounts(products) {
    return products.map(product => {
        if (Math.random() < 0.5) {
            product.oldPrice = product.price;
            product.price = +(product.price * 0.9).toFixed(2);
            product.isOnSale = true;
        } else {
            product.isOnSale = false;
        }
        product.rating = Math.round(product.rating);
        return product;
    });
}

// Фильтрация продуктов по строке поиска
export function filterProducts(products, searchString) {
    if (!searchString) return products;
    return products.filter(product => {
        const lowercasedSearchString = searchString.toLowerCase();
        return (
            product.title.toLowerCase().includes(lowercasedSearchString) ||
            product.price.toString().includes(lowercasedSearchString)
        );
    });
}

// Получение конкретного продукта по ID
export function getProductById(id) {
    return allProducts.find(product => product.id === Number(id));
}

// Получение локальных продуктов
export function getLocalProducts() {
    return localProducts;
}

// Добавление нового продукта
export async function addProduct(product) {
    product.id = generateNewId(); // Генерация нового ID для продукта
    localProducts.push(product); // Сохранение в локальном состоянии
    allProducts.push(product); // Добавляем в общий список
}

// Генерация нового ID
export function generateNewId() {
    return currentId++;
}
