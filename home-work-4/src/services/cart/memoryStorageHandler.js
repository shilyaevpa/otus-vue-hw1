import { reactive, computed } from 'vue';

const state = reactive({
    cart: [],
});

const memoryStorageHandler = {
    addToCart(product) {
        const existingProduct = state.cart.find((item) => item.id === product.id);
        if (existingProduct) {
            existingProduct.quantity += 1;
        } else {
            state.cart.push({ ...product, quantity: 1 });
        }
    },
    updateQuantity(productId, quantity) {
        const product = state.cart.find((item) => item.id === productId);
        if (product) {
            product.quantity = quantity;
            if (quantity <= 0) {
                this.removeFromCart(productId);
            }
        }
    },
    removeFromCart(productId) {
        state.cart = state.cart.filter((item) => item.id !== productId);
    },
    clearCart() {
        state.cart = [];
    },
    getCartItems() {
        return computed(() => state.cart);
    },
    getCartItemCount() {
        return computed(() =>
            state.cart.reduce((total, item) => total + item.quantity, 0)
        );
    },
    getTotalPrice() {
        return computed(() =>
            state.cart.reduce((total, item) => total + item.price * item.quantity, 0)
        );
    },
};

export default memoryStorageHandler;
