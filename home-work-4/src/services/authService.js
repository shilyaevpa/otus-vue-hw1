let isLoggedIn = false;

const authService = {
    login() {
        isLoggedIn = true;
    },
    logout() {
        isLoggedIn = false;
    },
    isAuthenticated() {
        return isLoggedIn;
    },
};

export default authService;
