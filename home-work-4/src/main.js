import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import authService from './services/authService'; // Имитация API авторизации
import cartService from '@/services/cart/cartService';
import memoryStorageHandler from '@/services/cart/memoryStorageHandler';
// Устанавливаем обработчик хранилища
cartService.setStorageHandler(memoryStorageHandler);

router.beforeEach((to, from, next) => {
    // Проверяем, требует ли маршрут авторизации
    if (to.meta.requiresAuth) {
        const isAuthenticated = authService.isAuthenticated(); // Проверяем авторизацию пользователя

        if (isAuthenticated) {
            next(); // Если авторизован, продолжаем
        } else {
            next({ name: 'login' }); // Если нет, перенаправляем на страницу логина
        }
    } else {
        next(); // Если авторизация не требуется, просто продолжаем
    }
});

const app = createApp(App)
app.use(router)
app.mount('#app')
