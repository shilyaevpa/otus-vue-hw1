import { createRouter, createWebHistory } from 'vue-router'
import CatalogSection from '@/components/CatalogSection.vue'
import Order from '@/components/OrderForm.vue'
import AddProduct from "@/components/AddProduct.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: CatalogSection
    },
    {
      path: '/order',
      name: 'order',
      component: Order
    },
    {
      path: '/catalog/add',
      name: 'add_product',
      component: AddProduct
    }
  ]
})

export default router
