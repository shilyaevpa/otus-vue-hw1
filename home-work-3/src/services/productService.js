let currentId = 2000;   //Чтобы не пересекались ИД с fake
let localProducts = [];

export async function fetchProducts() {
    const response = await fetch('https://fakestoreapi.com/products');
    if (!response.ok) {
        throw new Error('Failed to fetch products');
    }
    let products = await response.json();
    products = applyRandomDiscounts(products);
    return [...products, ...localProducts]; // Возвращаем комбинацию продуктов с сервера и локальных продуктов
}

function applyRandomDiscounts(products) {
    return products.map(product => {
        if (Math.random() < 0.5) {
            product.oldPrice = product.price;
            product.price = +(product.price * 0.9).toFixed(2);
            product.isOnSale = true;
        } else {
            product.isOnSale = false;
        }
        product.rating = Math.round(product.rating);
        return product;
    });
}

export function filterProducts(products, searchString) {
    if (!searchString) return products;
    return products.filter(product => {
        const lowercasedSearchString = searchString.toLowerCase();
        return product.title.toLowerCase().includes(lowercasedSearchString) ||
            product.price.toString().includes(lowercasedSearchString);
    });
}

export function getLocalProducts() {
    return localProducts;
}

export async function addProduct(product) {
    product.id = generateNewId(); // Генерация нового ID для продукта

    // Фиктивный запрос на сервер
    await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify(product),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    });

    localProducts.push(product); // Сохранение в локальном состоянии
}

export function generateNewId() {
    return currentId++;
}
