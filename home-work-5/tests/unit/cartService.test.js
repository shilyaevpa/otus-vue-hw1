import { describe, it, expect, beforeAll } from 'vitest';
import { setActivePinia, createPinia } from 'pinia';
import { useCartStore } from '@/stores/cartStore';
import cartService from '@/services/cart/cartService';
import piniaStorageHandler from '@/services/cart/piniaStorageHandler';

// Тестовый товар для тестов
const testProduct = { id: 1, title: "Test Product", price: 100, quantity: 1 };

describe('Cart Service', () => {
    let cartStore;

    beforeAll(() => {
        const pinia = createPinia();
        setActivePinia(pinia);
        cartStore = useCartStore();
        cartService.setStorageHandler(piniaStorageHandler);
        cartService.clearCart();    //Перед тестами все обнуляем
    });

    it('добавляет товар в корзину', () => {
        cartService.addToCart(testProduct);
        expect(cartStore.cart.length).toBe(1);
        expect(cartStore.cart[0]).toEqual(testProduct);
    });

    it('увеличивает количество товара, если он уже есть в корзине', () => {
        cartService.addToCart(testProduct);
        expect(cartStore.cart[0].quantity).toBe(2);
    });

    it('обновляет количество товара в корзине', () => {
        cartService.addToCart(testProduct);
        cartService.updateQuantity(testProduct.id, 5);
        expect(cartStore.cart[0].quantity).toBe(5);
    });

    it('удаляет товар из корзины', () => {
        cartService.addToCart(testProduct);
        cartService.removeFromCart(testProduct.id);
        expect(cartStore.cart.length).toBe(0);
    });

    it('очищает корзину', () => {
        cartService.addToCart(testProduct);
        cartService.addToCart({ id: 2, title: "Another Product", price: 50, quantity: 1 });
        cartService.clearCart();
        expect(cartStore.cart.length).toBe(0);
    });

    it('возвращает количество товаров в корзине', () => {
        cartService.addToCart(testProduct);
        cartService.addToCart({ id: 2, title: "Another Product", price: 50, quantity: 2 });
        expect(cartService.getCartItemCount()).toBe(2);
    });

    it('вычисляет общую сумму корзины', () => {
        cartService.addToCart(testProduct);
        cartService.addToCart({ id: 2, title: "Another Product", price: 50, quantity: 2 });
        expect(cartService.getTotalPrice()).toBe(300); // 100 + (50 * 2) + (50 * 2)
    });
});
