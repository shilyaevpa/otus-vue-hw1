import { describe, it, expect, vi, beforeAll } from 'vitest';
import authService from '@/services/authService';
import { useUserStore } from '@/stores/userStore';

// Мокаем хранилище
vi.mock('@/stores/userStore', () => ({
    useUserStore: vi.fn()
}));

describe('authService', () => {
    let userStore;

    beforeAll(() => {
        // Инициализируем мок хранилища
        userStore = {
            login: vi.fn(),
            logout: vi.fn(),
            isAuthenticated: false
        };

        useUserStore.mockReturnValue(userStore);
    });

    it('вызывает login при логине с нужным параметром', () => {
        const username = 'admin';

        // Вызываем метод сервиса
        authService.login(username);

        // Проверяем, что метод login хранилища был вызван
        expect(userStore.login).toHaveBeenCalledWith(username);
    });

    it('вызовыет logout при разлогинивании', () => {
        // Вызываем метод сервиса
        authService.logout();

        // Проверяем, что метод logout хранилища был вызван
        expect(userStore.logout).toHaveBeenCalled();
    });

    it('проверяет авторизацию', () => {

        userStore.isAuthenticated = true;

        expect(authService.isAuthenticated()).toBe(true); // Проверяем, что возвращается правильное значение

        userStore.isAuthenticated = false; // Проверяем обратный случай
        expect(authService.isAuthenticated()).toBe(false);
    });
});
