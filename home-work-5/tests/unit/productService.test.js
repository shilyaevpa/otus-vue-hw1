import { describe, it, expect, vi, beforeEach } from 'vitest';
import * as productService from '@/services/productService';

describe('Product Service', () => {
    let mockFetch;
    let mockProducts;

    beforeEach(() => {
        // Мокируем fetch для загрузки продуктов
        mockProducts = [
            { id: 1, title: 'Product 1', price: 100, rating: 5 },
            { id: 2, title: 'Product 2', price: 200, rating: 4 },
        ];
        mockFetch = vi.fn().mockResolvedValue({
            ok: true,
            json: vi.fn().mockResolvedValue(mockProducts),
        });

        global.fetch = mockFetch; // Переназначаем глобальный fetch
    });

    it('fetchProducts должен грузить и кэшировать продукты', async () => {
        // Загружаем продукты впервые
        const products = await productService.fetchProducts();
        expect(products).toEqual(mockProducts);
        expect(mockFetch).toHaveBeenCalledOnce();

        // Пробуем снова загрузить (должен использовать кэш)
        const cachedProducts = await productService.fetchProducts();
        expect(cachedProducts).toEqual(mockProducts);
        expect(mockFetch).toHaveBeenCalledTimes(1); // Fetch не должен быть вызван снова
    });

    it('filterProducts должен фильтровать по цене и названию', () => {
        const searchString = 'Product 1';
        const filteredProducts = productService.filterProducts(mockProducts, searchString);
        expect(filteredProducts.length).toBe(1);
        expect(filteredProducts[0].title).toBe('Product 1');
    });

    it('getProductById должен возвращать продукт с корректным набором полей', () => {
        const product = productService.getProductById(1);

        // Проверим, что продукт с таким id существует
        expect(product).toBeTruthy();

        // Проверим, что продукт имеет ожидаемые основные поля
        expect(product).toHaveProperty('id');
        expect(product).toHaveProperty('title');
        expect(product).toHaveProperty('price');
        expect(product).toHaveProperty('rating');

    });


    it('addProduct должен добавлять продукт', async () => {
        const newProduct = { title: 'New Product', price: 300, rating: 3 };
        await productService.addProduct(newProduct);
        const allProducts = await productService.fetchProducts(); // Получаем все продукты, включая новый
        expect(allProducts).toHaveLength(mockProducts.length + 1);
        expect(allProducts[allProducts.length - 1].title).toBe('New Product');
    });

    it('generateNewId должен генерировать уникальные не повторяющиеся ID', () => {
        const id1 = productService.generateNewId();
        const id2 = productService.generateNewId();
        expect(id1).not.toBe(id2);
    });
});
