describe('Корзина: добавление товаров', () => {
    it('Очищает корзину, добавляет товары и проверяет счетчик', () => {
        // 1. Переход на страницу корзины
        cy.visit('/cart');

        // 2. Проверяем, есть ли товары в корзине перед очисткой и удаляем все
        cy.get('body').then(($body) => {
            if ($body.find('.btn.btn-danger.btn-sm').length > 0) {
                // Если кнопки удаления есть, очищаем корзину
                cy.get('.btn.btn-danger.btn-sm').each(($btn) => {
                    cy.wrap($btn).click();
                });
            } else {
                // Если товаров нет, проверяем, что корзина пустая
                cy.contains('Корзина пуста').should('be.visible');
            }
        });

        // 3. Проверяем, что очищена
        cy.get('span.badge.bg-dark.text-white.ms-1.rounded-pill')
            .should('have.text', '0');

        // 4. Переход на страницу товаров
        cy.visit('/products');

        // 5. Добавляем первые 4 товара в корзину
        cy.get('.btn.btn-outline-dark.mt-auto').then(($buttons) => {
            Cypress._.take($buttons, 4).forEach((btn) => {
                cy.wrap(btn).click();
            });
        });

        // 6. Проверяем, что счетчик корзины показывает 4
        cy.get('span.badge.bg-dark.text-white.ms-1.rounded-pill')
            .should('have.text', '4');

        // 7. Переходим в корзину
        cy.visit('/cart');

        // Проверяем, что в корзине есть товары
        cy.get('.btn.btn-danger.btn-sm').should('exist');

        // Получаем текущее количество товаров в корзине
        cy.get('.badge.bg-dark.text-white.ms-1.rounded-pill')
            .invoke('text')
            .then((text) => {
                let initialCount = parseInt(text, 10); // Преобразуем текст в число

                // Удаляем товары по одному, проверяя обновление счетчика
                for (let i = initialCount; i > 0; i--) {
                    cy.get('.btn.btn-danger.btn-sm').first().click();

                    if (i - 1 > 0) {
                        // Проверяем, что счетчик уменьшился на 1
                        cy.get('.badge.bg-dark.text-white.ms-1.rounded-pill')
                            .should('have.text', (i - 1).toString());
                    } else {
                        // Если все товары удалены, проверяем, что корзина пуста
                        cy.contains('Корзина пуста').should('be.visible');
                    }
                }
            });
    });
});
