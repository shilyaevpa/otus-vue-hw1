import { useCartStore } from '@/stores/cartStore';

const piniaStorageHandler = {
    addToCart(product) {
        const store = useCartStore();
        store.addToCart(product);
    },
    updateQuantity(productId, quantity) {
        const store = useCartStore();
        store.updateQuantity(productId, quantity);
    },
    removeFromCart(productId) {
        const store = useCartStore();
        store.removeFromCart(productId);
    },
    clearCart() {
        const store = useCartStore();
        store.clearCart();
    },
    getCartItems() {
        const store = useCartStore();
        return store.cart; // Возвращаем прямо cart, так как он уже реактивный
    },
    getCartItemCount() {
        const store = useCartStore();
        return store.getCartItemCount; // Возвращаем геттер, это уже реактивное значение
    },
    getTotalPrice() {
        const store = useCartStore();
        return store.getTotalPrice; // Тоже возвращаем геттер напрямую
    },
};

export default piniaStorageHandler;
