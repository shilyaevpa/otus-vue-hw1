let storageHandler = null;

// Интерфейс для работы с корзиной
const cartService = {
    setStorageHandler(handler) {
        storageHandler = handler;
    },
    addToCart(product) {
        return storageHandler.addToCart(product);
    },
    updateQuantity(productId, quantity) {
        return storageHandler.updateQuantity(productId, quantity);
    },
    removeFromCart(productId) {
        return storageHandler.removeFromCart(productId);
    },
    clearCart() {
        return storageHandler.clearCart();
    },
    getCartItems() {
        return storageHandler.getCartItems();
    },
    getCartItemCount() {
        return storageHandler.getCartItemCount();
    },
    getTotalPrice() {
        return storageHandler.getTotalPrice();
    },
};

export default cartService;