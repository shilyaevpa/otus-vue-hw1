import { useUserStore } from '@/stores/userStore';

const authService = {
    login(username) {
        const userStore = useUserStore();
        userStore.login(username); // Используем действие login из хранилища
    },
    logout() {
        const userStore = useUserStore();
        userStore.logout(); // Используем действие logout из хранилища
    },
    isAuthenticated() {
        const userStore = useUserStore();
        return userStore.isAuthenticated; // Используем геттер isAuthenticated из хранилища
    },
};

export default authService;