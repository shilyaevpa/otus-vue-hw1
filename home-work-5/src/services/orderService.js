export const submitOrder = async (formData) => {
    try {
        const response = await fetch('https://httpbin.org/post', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });
        const data = await response.json();
        console.log('Server response:', data);
        return data;
    } catch (error) {
        console.error('Failed to submit order:', error);
        throw new Error('Failed to submit order');
    }
};
