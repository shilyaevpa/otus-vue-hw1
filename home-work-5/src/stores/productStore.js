import { defineStore } from 'pinia';

let currentId = 2000; // ID для локальных товаров

export const useProductStore = defineStore('product', {
    state: () => ({
        products: [], // Храним все загруженные продукты
        localProducts: [], // Храним локально добавленные продукты
    }),
    actions: {
        async fetchProducts() {
            // Если товары уже загружены, возвращаем их
            if (this.products.length > 0) {
                return this.products;
            }

            try {
                const response = await fetch('https://fakestoreapi.com/products');
                if (!response.ok) {
                    throw new Error('Failed to fetch products');
                }

                let products = await response.json();
                products = this.applyRandomDiscounts(products);

                // Объединяем с локальными продуктами
                this.products = [...products, ...this.localProducts];
                return this.products;
            } catch (error) {
                console.error('Ошибка загрузки продуктов:', error);
                return [];
            }
        },

        addProduct(product) {
            product.id = this.generateNewId();
            this.localProducts.push(product);
            this.products.push(product); // Добавляем в общий список
        },

        filterProducts(searchString) {
            if (!searchString) return this.products;
            const lowercasedSearchString = searchString.toLowerCase();
            return this.products.filter(product =>
                product.title.toLowerCase().includes(lowercasedSearchString) ||
                product.price.toString().includes(lowercasedSearchString)
            );
        },

        getProductById(id) {
            return this.products.find(product => product.id === Number(id));
        },

        applyRandomDiscounts(products) {
            return products.map(product => {
                if (Math.random() < 0.5) {
                    product.oldPrice = product.price;
                    product.price = +(product.price * 0.9).toFixed(2);
                    product.isOnSale = true;
                } else {
                    product.isOnSale = false;
                }
                product.rating = {
                    rate: Math.round(Math.random() * 5)
                };
                return product;
            });
        },

        generateNewId() {
            return currentId++;
        },
    },
    persist: true, // Включаем сохранение в localStorage
});
