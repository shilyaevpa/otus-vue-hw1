import { defineStore } from 'pinia';

export const useCartStore = defineStore('cart', {
    state: () => ({
        cart: [],
    }),
    actions: {
        addToCart(product) {
            const existingProduct = this.cart.find((item) => item.id === product.id);
            if (existingProduct) {
                existingProduct.quantity += 1;
            } else {
                this.cart.push({ ...product, quantity: 1 });
            }
        },
        updateQuantity(productId, quantity) {
            const product = this.cart.find((item) => item.id === productId);
            if (product) {
                product.quantity = quantity;
                if (quantity <= 0) {
                    this.removeFromCart(productId);
                }
            }
        },
        removeFromCart(productId) {
            this.cart = this.cart.filter((item) => item.id !== productId);
        },
        clearCart() {
            this.cart = [];
        },
    },
    getters: {
        getCartItemCount: (state) =>
            state.cart.reduce((total, item) => total + item.quantity, 0),
        getTotalPrice: (state) =>
            state.cart.reduce((total, item) => total + item.price * item.quantity, 0),
    },
    persist: true, // Включаем сохранение в localStorage
});
