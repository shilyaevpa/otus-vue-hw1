import { defineStore } from 'pinia';

export const useUserStore = defineStore('user', {
    state: () => ({
        username: null,
        isAuthenticated: false,
    }),
    actions: {
        login(username) {
            this.username = username;
            this.isAuthenticated = true;
        },
        logout() {
            this.username = null;
            this.isAuthenticated = false;
        },
    },
});