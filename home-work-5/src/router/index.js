import { createRouter, createWebHistory } from 'vue-router'
import CatalogSection from '@/components/catalog/CatalogSection.vue'
import Order from '@/components/order/OrderForm.vue'
import AddProduct from "@/components/catalog/AddProduct.vue";
import Cart from "@/components/order/CartContent.vue";
import ProductDetail from '@/components/catalog/ProductDetail.vue';
import About from "@/components/content/AboutPage.vue";
import LogonPage from "@/components/LogonPage.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: CatalogSection
    },
    {
      path: '/login',
      name: 'login',
      component: LogonPage,
      meta: {
        title: 'Авторизация'
      }
    },
    {
      path: '/products',
      name: 'catalog',
      component: CatalogSection,
      meta: {
        title: 'Каталог'
      }
    },
    {
      path: '/popular',
      name: 'popular',
      component: CatalogSection,
      meta: {
        title: 'Популярные товары'
      }
    },
    {
      path: '/new',
      name: 'new',
      component: CatalogSection,
      meta: {
        title: 'Новинки'
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        title: 'О проекте'
      }
    },
    {
      path: '/order',
      name: 'order',
      component: Order,
      meta: {
        title: 'Оформление заказа'
      }
    },
    {
      path: '/catalog/add',
      name: 'add_product',
      component: AddProduct,
      meta: {
        title: 'Добавление товара',
        requiresAuth: true
      }
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
      meta: {
        title: 'Корзина'
      }
    },
    {
      path: '/product/:id',
      name: 'product_detail',
      component: ProductDetail,
      props: (route) => ({ productId: Number(route.params.id) })
    }
  ]
})

export default router
