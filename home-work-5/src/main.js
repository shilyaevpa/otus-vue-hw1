import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

import authService from './services/authService'; // Имитация API авторизации
import cartService from '@/services/cart/cartService';
import piniaStorageHandler from '@/services/cart/piniaStorageHandler';
// Устанавливаем обработчик хранилища
cartService.setStorageHandler(piniaStorageHandler);

router.beforeEach((to, from, next) => {
    // Проверяем, требует ли маршрут авторизации
    if (to.meta.requiresAuth) {
        const isAuthenticated = authService.isAuthenticated(); // Проверяем авторизацию пользователя

        if (isAuthenticated) {
            next(); // Если авторизован, продолжаем
        } else {
            next({ name: 'login' }); // Если нет, перенаправляем на страницу логина
        }
    } else {
        next(); // Если авторизация не требуется, просто продолжаем
    }
});

const app = createApp(App)
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

app.use(pinia);
app.use(router)
app.mount('#app')
