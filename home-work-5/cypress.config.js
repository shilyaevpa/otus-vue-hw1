import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    specPattern: "tests/e2e/**/*.cy.js",
    baseUrl: 'http://localhost:5173/',
    supportFile: false
  },

  component: {
    devServer: {
      framework: "vue",
      bundler: "vite",
    },
  },
});
